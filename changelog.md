# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2024-12-02
### Added
- Added seeding component for identity attributes.
- Added documentation for seeding configuration.
- Added `enabled` and `filePath` properties to configure seeding procedure.
- Added column `is_right` to table `identity_attribute`.

### Changed
- Renamed attribute `participantType` to `participantTypes` in `SecurityAttributesMappingDTO`.
- Column `is_right` of `identity_attribute` is not nullable anymore and has "false" as default value.

### Fixed
- Fix unit testing.

## [0.7.0] - 2024-11-11
### Changed
- Adapt logic to use `CredentialId` instead of `ParticipantId`
### Fixed
- Fix `credentialId` instead of `certificateId`
### Removed
- Remove `participantType` enum

