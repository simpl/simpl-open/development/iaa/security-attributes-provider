package eu.europa.ec.simpl.securityattributesprovider.mappers;

import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.securityattributesprovider.dto.SecurityAttributesMappingDTO;
import eu.europa.ec.simpl.securityattributesprovider.entities.IdentityAttribute;
import eu.europa.ec.simpl.securityattributesprovider.projection.IdentityAttributeWithOwnership;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;

public class IdentityAttributeMapperTest {
    private final IdentityAttributeMapper mapper = new IdentityAttributeMapperImpl();

    @Test
    void toEntityTest() {
        mapper.toEntity(Instancio.create(SecurityAttributesMappingDTO.class));
        mapper.toEntity(Instancio.create(IdentityAttributeDTO.class));
    }

    @Test
    void toDtoTest() {
        mapper.toDto(Instancio.create(IdentityAttribute.class));
    }

    @Test
    void toLightDtoTest() {
        mapper.toLightDto(Instancio.create(IdentityAttribute.class));
    }

    @Test
    void updateEntityTest() {
        mapper.updateEntity(Instancio.create(IdentityAttributeDTO.class), Instancio.create(IdentityAttribute.class));
    }

    @Test
    void toDtoWithOwnershipTest() {
        mapper.toDtoWithOwnership(Instancio.create(IdentityAttributeWithOwnership.class));
    }
}
