package eu.europa.ec.simpl.securityattributesprovider.services.impl;

import static eu.europa.ec.simpl.common.test.TestDataUtil.aPageOf;
import static eu.europa.ec.simpl.common.test.TestDataUtil.aPageable;
import static eu.europa.ec.simpl.common.test.TestUtil.*;
import static org.assertj.core.api.Assertions.*;
import static org.instancio.Select.field;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.exchanges.identityprovider.IdentityProviderParticipantExchange;
import eu.europa.ec.simpl.common.exchanges.onboarding.ParticipantTypeExchange;
import eu.europa.ec.simpl.common.filters.IdentityAttributeFilter;
import eu.europa.ec.simpl.common.filters.ParticipantFilter;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import eu.europa.ec.simpl.securityattributesprovider.entities.IdentityAttribute;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.IdentityAttributeAlreadyExistException;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.IdentityAttributeAssignedException;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.IdentityAttributeNotFoundException;
import eu.europa.ec.simpl.securityattributesprovider.mappers.IdentityAttributeMapperImpl;
import eu.europa.ec.simpl.securityattributesprovider.projection.IdentityAttributeWithOwnership;
import eu.europa.ec.simpl.securityattributesprovider.repositories.IdentityAttributeRepository;
import eu.europa.ec.simpl.securityattributesprovider.repositories.specifications.IdentityAttributeSpecification;
import eu.europa.ec.simpl.securityattributesprovider.services.ParticipantTypeService;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;
import org.instancio.Instancio;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class IdentityAttributeServiceImplTest {

    @Mock
    IdentityAttributeRepository repository;

    @Spy
    IdentityAttributeMapperImpl mapper;

    @Mock
    IdentityProviderParticipantExchange participantExchange;

    @Mock
    ParticipantTypeExchange participantTypeExchange;

    @Mock
    ParticipantTypeService participantTypeService;

    @InjectMocks
    IdentityAttributeServiceImpl service;

    @Test
    void create_whenIdentityAttributeDoesNotExist_shouldSucceed() {
        // Given
        given(repository.existsByCodeOrName(any(), any())).willReturn(false);
        given(repository.saveAll(anyList())).willAnswer(returnsFirstArg());
        // When
        service.create(a(IdentityAttributeDTO.class));
        // Then
        then(repository).should().saveAll(anyList());
    }

    @Test
    void create_whenIdentityAttributeAlreadyExists_shouldThrowIdentityAttributeAlreadyExistException() {
        // Given
        given(repository.existsByCodeOrName(any(), any())).willReturn(true);
        // When
        var exception = catchException(() -> service.create(a(IdentityAttributeDTO.class)));
        // Then
        then(repository).should(never()).saveAll(any());
        assertThat(exception).isInstanceOf(IdentityAttributeAlreadyExistException.class);
    }

    @ParameterizedTest
    @InstancioSource
    void create_withParticipantTypes_shouldCheckWhetherThoseParticipantTypesExist(
            IdentityAttributeDTO identityAttribute) {
        // Given
        given(repository.existsByCodeOrName(any(), any())).willReturn(false);
        given(repository.saveAll(anyList())).willAnswer(returnsFirstArg());
        // When
        service.create(identityAttribute);
        // Then
        then(participantTypeService).should().checkExistingOrThrow(any());
    }

    @ParameterizedTest
    @InstancioSource
    void create_withoutParticipantTypes_shouldNotCallParticipantTypeExchange(IdentityAttributeDTO identityAttribute) {
        // Given
        given(repository.existsByCodeOrName(any(), any())).willReturn(false);
        given(repository.saveAll(anyList())).willAnswer(returnsFirstArg());
        identityAttribute.setParticipantTypes(null);
        // When
        service.create(identityAttribute);
        // Then
        then(participantTypeExchange).shouldHaveNoInteractions();
    }

    @Test
    void findById() {
        // Given
        IdentityAttribute identityAttribute =
                Instancio.of(IdentityAttribute.class).create();
        given(repository.findFullById(any())).willReturn(Optional.of(identityAttribute));
        // When
        service.findById(identityAttribute.getId());
        // Then
        then(repository).should().findFullById(identityAttribute.getId());
    }

    @Test
    void findByIdNotFound() {
        var id = anUUID();
        assertThrows(IdentityAttributeNotFoundException.class, () -> service.findById(id));
    }

    @Test
    void update() {
        // Given
        IdentityAttribute identityAttribute =
                Instancio.of(IdentityAttribute.class).create();
        given(repository.findFullByIdOrThrow(any())).willReturn(identityAttribute);

        // When
        IdentityAttributeDTO identityAttributeDTO =
                Instancio.of(IdentityAttributeDTO.class).create();
        service.update(UUID.randomUUID(), identityAttributeDTO);

        // Then
        assertEquals(identityAttributeDTO.getCode(), identityAttribute.getCode());
    }

    @Test
    void updateNotFound() {
        // Given
        given(repository.findFullByIdOrThrow(any())).willThrow(IdentityAttributeNotFoundException.class);

        // When
        IdentityAttributeDTO identityAttributeDTO =
                Instancio.of(IdentityAttributeDTO.class).create();

        var uuid = UUID.randomUUID();
        assertThrows(IdentityAttributeNotFoundException.class, () -> service.update(uuid, identityAttributeDTO));
    }

    @Test
    void whenDeletingNotAssignedAttributeThenOk() {
        UUID uuid = UUID.randomUUID();
        IdentityAttribute attr = Instancio.of(IdentityAttribute.class)
                .set(field(IdentityAttribute::isUsed), false)
                .create();

        when(repository.findById(uuid)).thenReturn(Optional.of(attr));

        service.delete(uuid);

        verify(repository, times(1)).deleteById(uuid);
    }

    @Test
    void whenDeletingInvalidAttributeThenNotFound() {
        UUID uuid = UUID.randomUUID();
        when(repository.findById(uuid)).thenReturn(Optional.empty());

        assertThrows(IdentityAttributeNotFoundException.class, () -> service.delete(uuid));
    }

    @Test
    void whenDeletingAssignedAttributeThenNotAllowed() {
        UUID uuid = UUID.randomUUID();
        IdentityAttribute attr = Instancio.of(IdentityAttribute.class)
                .set(field(IdentityAttribute::isUsed), true)
                .create();

        when(repository.findById(uuid)).thenReturn(Optional.of(attr));

        assertThrows(IdentityAttributeAssignedException.class, () -> service.delete(uuid));
    }

    @Test
    void search() {
        // Given
        given(repository.findAll(any(IdentityAttributeSpecification.class), any(Pageable.class)))
                .willReturn(
                        new PageImpl<>(Instancio.ofList(IdentityAttribute.class).create()));
        // When
        service.search(an(IdentityAttributeFilter.class), aPageable());
        // Then
        then(repository).should().findAll(any(IdentityAttributeSpecification.class), any(Pageable.class));
    }

    @Test
    void retrieveSnapshot() {
        // Given
        given(repository.retrieveSnapshot(any(), any())).willReturn(aSetOf(UUID.class));

        // When
        String type = "CONSUMER";
        Set<UUID> identityAttributes = repository.retrieveSnapshot(type, List.of());

        // Then
        then(repository).should().retrieveSnapshot(type, List.of());

        assertThat(identityAttributes).isNotEmpty();
    }

    @Test
    void updateAssignableParameter() {

        var ids = Stream.of("c044b70a-6839-421a-9247-a14ecff326a3", "223bc901-6202-4de6-a0d7-c1e2ca858e8d")
                .map(UUID::fromString)
                .toList();
        when(repository.existsById(any())).thenReturn(true);
        service.updateAssignableParameter(ids, true);

        verify(repository).updateAssignableParameter(ids, true);
    }

    @Test
    void updateAssignableParameter_notFound() {

        var ids = Stream.of("c044b70a-6839-421a-9247-a14ecff326a3", "223bc901-6202-4de6-a0d7-c1e2ca858e8d")
                .map(UUID::fromString)
                .toList();
        when(repository.existsById(any())).thenReturn(false);
        assertThrows(IdentityAttributeNotFoundException.class, () -> service.updateAssignableParameter(ids, true));
    }

    @ParameterizedTest
    @InstancioSource
    void getIdentityAttributesWithOwnership(String credentialId, UUID participantId) {
        // Given
        given(repository.findAllWithOwnershipByParticipantId(participantId))
                .willReturn(aListOf(IdentityAttributeWithOwnership.class));
        given(participantExchange.search(any(), any()))
                .willReturn(new PageResponse<>(
                        List.of(new ParticipantDTO().setId(participantId).setCredentialId(credentialId)),
                        a(PageResponse.PageMetadata.class)));

        // When
        service.getIdentityAttributesWithOwnership(credentialId);
        // Then
        then(repository).should().findAllWithOwnershipByParticipantId(participantId);
    }

    @ParameterizedTest
    @InstancioSource
    void getIdentityAttributesByCertificateId(String certificateId) {
        // Given
        given(participantExchange.search(any(), any())).willReturn(new PageResponse<>(aPageOf(ParticipantDTO.class)));
        given(repository.findAll(any(IdentityAttributeSpecification.class), any(Pageable.class)))
                .willReturn(aPageOf(IdentityAttribute.class));
        // When
        service.getIdentityAttributesByCertificateId(certificateId);
        // Then
        then(participantExchange).should().search(eq(new ParticipantFilter().setCredentialId(certificateId)), any());
    }

    @Test
    void creatingAllNewAttributesInBatch() {
        int attrToSave = 5;
        var list = Instancio.ofList(IdentityAttributeDTO.class).size(attrToSave).create();
        var savedList =
                Instancio.ofList(IdentityAttribute.class).size(attrToSave).create();

        when(repository.saveAll(anyList())).thenReturn(savedList);

        service.createBatch(list);

        ArgumentCaptor<List<IdentityAttribute>> savingList = ArgumentCaptor.forClass(List.class);
        verify(repository).saveAll(savingList.capture());

        var value = savingList.getValue();
        assertThat(value).hasSize(attrToSave);
    }

    @ParameterizedTest
    @InstancioSource
    void addParticipantType(List<UUID> identityattributeIds, String participantType) {
        // When
        service.addParticipantType(identityattributeIds, participantType);
        // Then
        then(repository).should().addParticipantType(identityattributeIds.toArray(UUID[]::new), participantType);
    }

    @ParameterizedTest
    @InstancioSource
    void unassign(UUID participantId, List<UUID> identityAttributes) {
        // When
        service.unassign(participantId, identityAttributes);
        // Then
        then(repository).should().unassignIdentityAttribute(participantId, identityAttributes);
    }

    @ParameterizedTest
    @InstancioSource
    void assign(UUID participantId, List<UUID> identityAttributeIds) {
        // When
        service.assign(participantId, identityAttributeIds);
        // Then
        then(repository).should().assignIdentityAttributes(participantId, identityAttributeIds.toArray(UUID[]::new));
    }

    @ParameterizedTest
    @InstancioSource
    void assignLastSnapshot(
            String participantType,
            UUID participantId,
            List<UUID> excludedIdentityAttributesIds,
            Set<UUID> identityAttributesToAssign) {

        given(repository.retrieveSnapshot(participantType, excludedIdentityAttributesIds))
                .willReturn(identityAttributesToAssign);
        // When
        service.assignLastSnapshot(participantType, participantId, excludedIdentityAttributesIds);
        // Then
        then(repository).should().retrieveSnapshot(participantType, excludedIdentityAttributesIds);
        then(repository)
                .should()
                .assignIdentityAttributes(participantId, identityAttributesToAssign.toArray(UUID[]::new));
    }
}
