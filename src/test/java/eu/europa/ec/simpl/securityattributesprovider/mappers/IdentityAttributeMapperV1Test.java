package eu.europa.ec.simpl.securityattributesprovider.mappers;

import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.SearchFilterParameterDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import java.util.List;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;

public class IdentityAttributeMapperV1Test {
    private final IdentityAttributeMapperV1 mapper = new IdentityAttributeMapperV1Impl();

    @Test
    void toV0Test() {
        mapper.toV0(Instancio.create(
                eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeDTO.class));
        mapper.toV0(Instancio.create(SearchFilterParameterDTO.class));
        mapper.toV0(List.of(Instancio.create(
                eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeDTO.class)));
    }

    @Test
    void toV1Test() {
        mapper.toV1(Instancio.create(IdentityAttributeDTO.class));
        mapper.toV1(List.of(Instancio.create(IdentityAttributeDTO.class)));
        mapper.toV1(Instancio.create(
                eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO
                        .class));
    }

    @Test
    void toV1IdentityAttributeWithOwnershipDTOTest() {
        mapper.toV1IdentityAttributeWithOwnershipDTO(List.of(Instancio.create(
                eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO
                        .class)));
    }
}
