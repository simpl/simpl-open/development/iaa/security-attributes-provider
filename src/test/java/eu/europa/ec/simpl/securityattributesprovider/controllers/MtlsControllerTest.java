package eu.europa.ec.simpl.securityattributesprovider.controllers;

import static org.mockito.BDDMockito.then;

import eu.europa.ec.simpl.securityattributesprovider.services.IdentityAttributeService;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MtlsControllerTest {

    @Mock
    IdentityAttributeService identityAttributeService;

    @InjectMocks
    MtlsController mtlsController;

    @ParameterizedTest
    @InstancioSource
    void getIdentityAttributesWithOwnership(String credentialId) {
        mtlsController.getIdentityAttributesWithOwnership(credentialId);
        then(identityAttributeService).should().getIdentityAttributesWithOwnership(credentialId);
    }

    @ParameterizedTest
    @InstancioSource
    void getIdentityAttributesByCertificateIdInUri(String credentialId) {
        mtlsController.getIdentityAttributesByCertificateIdInUri(credentialId);
        then(identityAttributeService).should().getIdentityAttributesByCertificateId(credentialId);
    }
}
