package eu.europa.ec.simpl.securityattributesprovider.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.aListOf;
import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.filters.IdentityAttributeFilter;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.test.WithMockSecurityContext;
import eu.europa.ec.simpl.securityattributesprovider.services.IdentityAttributeService;
import java.util.Arrays;
import java.util.UUID;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
class IdentityAttributeControllerTest {

    MockMvc mockMvc;

    @Mock
    IdentityAttributeService identityAttributeService;

    @InjectMocks
    IdentityAttributeController identityAttributeController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(identityAttributeController).build();
    }

    @Test
    @WithMockSecurityContext(roles = "IATTR_M")
    void create() {
        // When
        IdentityAttributeDTO identityAttributeDTO =
                Instancio.of(IdentityAttributeDTO.class).create();
        identityAttributeController.create(identityAttributeDTO);
        // Then
        then(identityAttributeService).should().create(identityAttributeDTO);
    }

    @Test
    @WithMockSecurityContext(roles = "IATTR_M")
    void findById() {
        // When
        UUID identityAttributeId = UUID.randomUUID();
        identityAttributeController.findById(identityAttributeId);
        // Then
        then(identityAttributeService).should().findById(identityAttributeId);
    }

    @Test
    @WithMockSecurityContext(roles = "IATTR_M")
    void updateAttributes() {
        // When
        UUID identityAttributeId = UUID.randomUUID();
        IdentityAttributeDTO updatedIdentityAttributeDTO =
                Instancio.of(IdentityAttributeDTO.class).create();
        identityAttributeController.updateAttributes(identityAttributeId, updatedIdentityAttributeDTO);

        // Then
        then(identityAttributeService).should().update(identityAttributeId, updatedIdentityAttributeDTO);
    }

    @Test
    @WithMockSecurityContext(roles = "IATTR_M")
    void delete() {
        // When
        UUID id = UUID.randomUUID();
        identityAttributeController.delete(id);
        // Then
        then(identityAttributeService).should().delete(id);
    }

    @Test
    @WithMockSecurityContext(roles = "IATTR_M")
    void searchIATTR_M() {
        testSearch();
    }

    @Test
    @WithMockSecurityContext(roles = "NOTARY")
    void searchNOTARY() {
        testSearch();
    }

    private void testSearch() {
        // When
        given(identityAttributeService.search(any(), any()))
                .willReturn(new PageImpl<>(aListOf(IdentityAttributeDTO.class)));

        identityAttributeController.search(an(IdentityAttributeFilter.class), Pageable.ofSize(10));

        // Then
        then(identityAttributeService).should().search(any(), any());
    }

    @Test
    void updateAssignableParameter() throws Exception {
        var ids = Arrays.asList("c044b70a-6839-421a-9247-a14ecff326a3", "223bc901-6202-4de6-a0d7-c1e2ca858e8d");

        mockMvc.perform(put("/identity-attribute/assignable/true")
                        .header("Content-type", MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(ids)))
                .andExpect(status().is(200));

        verify(identityAttributeService)
                .updateAssignableParameter(
                        argThat(body ->
                                body.stream().map(UUID::toString).toList().containsAll(ids)),
                        eq(true));
    }
}
