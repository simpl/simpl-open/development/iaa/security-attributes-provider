package eu.europa.ec.simpl.securityattributesprovider.services.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.exchanges.onboarding.ParticipantTypeExchange;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.securityattributesprovider.configurations.DBSeedingProperties;
import eu.europa.ec.simpl.securityattributesprovider.entities.IdentityAttribute;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.ParticipantTypeDoesNotExistException;
import eu.europa.ec.simpl.securityattributesprovider.mappers.IdentityAttributeMapperImpl;
import eu.europa.ec.simpl.securityattributesprovider.repositories.IdentityAttributeRepository;
import jakarta.validation.Validator;
import java.util.List;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties(DBSeedingProperties.class)
@TestPropertySource(
        properties = {
            "database-seeding.securityAttributeProviderMapping.enabled=true",
            "database-seeding.securityAttributeProviderMapping.filePath=db.seeding/identityAttributes.default.json",
        })
@Import(IdentityAttributeMapperImpl.class)
class SecurityAttributeProviderInitializationImplTest {

    @Autowired
    private ApplicationContext applicationContext;

    @MockitoBean
    private Validator validator;

    @MockitoBean
    private IdentityAttributeRepository identityAttributeRepository;

    @MockitoBean
    private ParticipantTypeExchange participantTypeExchange;

    @Mock
    private DBSeedingProperties dbSeedingProperties;

    @Test
    void initDatabaseTest_success() {
        var arguments = ArgumentCaptor.forClass(IdentityAttribute.class);
        var validParticipantTypes = getValidParticipantTypes();
        when(participantTypeExchange.getParticipantTypes()).thenReturn(validParticipantTypes);
        initializationSecurityAttributes();
        Mockito.verify(this.identityAttributeRepository, times(7)).save(arguments.capture());
        assertEquals(7, arguments.getAllValues().size());

        var ia = arguments.getAllValues().getFirst();
        assertEquals("Consumer", ia.getName());
        assertEquals("CONSUMER", ia.getCode());
        assertEquals(
                "Identity attribute used for tagging an end user able to act as a user of a data consumer participant",
                ia.getDescription());
        assertFalse(ia.isAssignableToRoles());
        assertFalse(ia.isRight());
        assertEquals("CONSUMER", ia.getParticipantTypes().iterator().next());

        ia = arguments.getAllValues().getLast();
        assertEquals("Infrastructure Provider Publisher", ia.getName());
        assertEquals("INFRA_PROVIDER_PUBLISHER", ia.getCode());
        assertEquals("Identity attribute needed for publishing Infrastructure", ia.getDescription());
        assertTrue(ia.isAssignableToRoles());
        assertTrue(ia.isRight());
        assertEquals(
                "INFRASTRUCTURE_PROVIDER", ia.getParticipantTypes().iterator().next());
    }

    @Test
    void initDatabaseTest_InvalidParticipantType_ThrowException() {
        var validParticipantTypes = Set.of(createParticipantTypeDTO("JUNIT_TEST_VALUE"));
        when(participantTypeExchange.getParticipantTypes()).thenReturn(validParticipantTypes);
        var ex = Assertions.catchException(this::initializationSecurityAttributes);
        Assertions.assertThat(ex).hasCauseInstanceOf(ParticipantTypeDoesNotExistException.class);
    }

    @Test
    void initDatabaseTest_TableIsNoEmpty_SkipInitialization() {
        var mockIdentityAttribute = new IdentityAttribute();
        BDDMockito.given(identityAttributeRepository.count()).willReturn(1L);
        when(identityAttributeRepository.findAll()).thenReturn(List.of(mockIdentityAttribute));
        initializationSecurityAttributes();
        verify(this.identityAttributeRepository, never()).save(any());
    }

    private void initializationSecurityAttributes() {
        try (var testContext = new AnnotationConfigApplicationContext()) {
            testContext.setParent(applicationContext);
            testContext.register(SecurityAttributeProviderInitializationImpl.class);
            testContext.refresh();
        }
    }

    private Set<ParticipantTypeDTO> getValidParticipantTypes() {
        return Set.of(
                createParticipantTypeDTO("APPLICATION_PROVIDER"),
                createParticipantTypeDTO("CONSUMER"),
                createParticipantTypeDTO("DATA_PROVIDER"),
                createParticipantTypeDTO("INFRASTRUCTURE_PROVIDER"));
    }

    private ParticipantTypeDTO createParticipantTypeDTO(String value) {
        var pt = new ParticipantTypeDTO();
        pt.setValue(value);
        return pt;
    }
}
