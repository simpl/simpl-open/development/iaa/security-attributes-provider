package eu.europa.ec.simpl.securityattributesprovider.services.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;
import static org.mockito.BDDMockito.*;

import eu.europa.ec.simpl.common.exchanges.onboarding.ParticipantTypeExchange;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.ParticipantTypeDoesNotExistException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParticipantTypeServiceImplTest {

    @Mock
    ParticipantTypeExchange participantTypeExchange;

    @InjectMocks
    ParticipantTypeServiceImpl participantTypeService;

    @ParameterizedTest
    @InstancioSource
    void checkExisting_withExistingOrThrowParticipantTypes_shouldSucceed(
            Set<ParticipantTypeDTO> existingParticipantTypes) {
        // Given
        given(participantTypeExchange.getParticipantTypes()).willReturn(existingParticipantTypes);
        var givenParticipantTypes = Set.of(existingParticipantTypes.stream()
                .map(ParticipantTypeDTO::getValue)
                .toList()
                .getFirst());

        // When
        var ex =
                catchException(() -> participantTypeService.checkExistingOrThrow(new HashSet<>(givenParticipantTypes)));
        // Then
        then(participantTypeExchange).should().getParticipantTypes();

        assertThat(ex).isNull();
    }

    @ParameterizedTest
    @InstancioSource
    void checkExisting_withNonExistingOrThrowParticipantTypes_shouldThrow_ParticipantTypeDoesNotExistException(
            Set<ParticipantTypeDTO> existingParticipantTypes) {
        // Given
        given(participantTypeExchange.getParticipantTypes()).willReturn(existingParticipantTypes);
        var givenParticipantTypes = new ArrayList<>(existingParticipantTypes.stream()
                .map(ParticipantTypeDTO::getValue)
                .toList());
        givenParticipantTypes.set(0, "DIFFERENT_VALUE");

        // When
        var ex =
                catchException(() -> participantTypeService.checkExistingOrThrow(new HashSet<>(givenParticipantTypes)));
        // Then
        then(participantTypeExchange).should().getParticipantTypes();

        assertThat(ex).isInstanceOf(ParticipantTypeDoesNotExistException.class);
    }

    @Test
    void checkExisting_OrThrow_withEmptyParticipantTypes_shouldSucceed() {
        // When
        var ex = catchException(() -> participantTypeService.checkExistingOrThrow(Set.of()));
        // Then
        assertThat(ex).isNull();

        then(participantTypeExchange).shouldHaveNoInteractions();
    }

    @Test
    @SuppressWarnings("all")
    void checkExisting_OrThrow_withNullParticipantTypes_shouldThrowNullPointerException() {
        // When
        var exception = catchException(() -> participantTypeService.checkExistingOrThrow(null));
        // Then
        assertThat(exception).isInstanceOf(NullPointerException.class);
        then(participantTypeExchange).shouldHaveNoInteractions();
    }
}
