package eu.europa.ec.simpl.securityattributesprovider.services.impl;

import static org.mockito.Mockito.verifyNoInteractions;

import eu.europa.ec.simpl.common.exchanges.onboarding.ParticipantTypeExchange;
import eu.europa.ec.simpl.securityattributesprovider.configurations.DBSeedingProperties;
import eu.europa.ec.simpl.securityattributesprovider.mappers.IdentityAttributeMapperImpl;
import eu.europa.ec.simpl.securityattributesprovider.repositories.IdentityAttributeRepository;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties(DBSeedingProperties.class)
@TestPropertySource(
        properties = {
            "database-seeding.securityAttributeProviderMapping.enabled=false",
            "database-seeding.securityAttributeProviderMapping.filePath=db.seeding/identityAttributes.default.json",
        })
@Import(IdentityAttributeMapperImpl.class)
class SkipSecurityAttributeProviderInitializationImplTest {

    @Autowired
    private ApplicationContext applicationContext;

    @MockitoBean
    private IdentityAttributeRepository identityAttributeRepository;

    @MockitoBean
    private Validator validator;

    @MockitoBean
    private ParticipantTypeExchange participantTypeExchange;

    @Test
    void initDatabaseTest_skip() {
        initializationSecurityAttributes();
        verifyNoInteractions(identityAttributeRepository);
    }

    private void initializationSecurityAttributes() {
        try (var testContext = new AnnotationConfigApplicationContext()) {
            testContext.setParent(applicationContext);
            testContext.register(SecurityAttributeProviderInitializationImpl.class);
            testContext.refresh();
        }
    }
}
