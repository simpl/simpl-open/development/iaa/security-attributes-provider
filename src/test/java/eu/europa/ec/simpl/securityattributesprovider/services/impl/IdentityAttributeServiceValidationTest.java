package eu.europa.ec.simpl.securityattributesprovider.services.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;
import static org.assertj.core.api.InstanceOfAssertFactories.type;
import static org.mockito.BDDMockito.then;

import eu.europa.ec.simpl.common.exchanges.identityprovider.IdentityProviderParticipantExchange;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.securityattributesprovider.mappers.IdentityAttributeMapperImpl;
import eu.europa.ec.simpl.securityattributesprovider.repositories.IdentityAttributeRepository;
import eu.europa.ec.simpl.securityattributesprovider.services.IdentityAttributeService;
import eu.europa.ec.simpl.securityattributesprovider.services.ParticipantTypeService;
import jakarta.validation.ConstraintViolationException;
import java.util.List;
import java.util.UUID;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.bean.override.mockito.MockitoBean;

@SpringBootTest(classes = {IdentityAttributeServiceImpl.class, ValidationAutoConfiguration.class})
class IdentityAttributeServiceValidationTest {

    @MockitoBean
    IdentityAttributeRepository repository;

    @MockitoBean(answers = Answers.CALLS_REAL_METHODS)
    IdentityAttributeMapperImpl mapper;

    @MockitoBean
    IdentityProviderParticipantExchange participantExchange;

    @MockitoBean
    ParticipantTypeService participantTypeService;

    @Autowired
    IdentityAttributeService service;

    @ParameterizedTest
    @InstancioSource
    void create_withAnInvalidIdentityAttribute_shouldThrowConstraintViolationException(
            IdentityAttributeDTO identityAttribute) {
        // Given
        var violationsCount = 4;
        identityAttribute.setId(null);
        identityAttribute.setName(null);
        identityAttribute.setCode(null);
        identityAttribute.setEnabled(null);
        identityAttribute.setAssignableToRoles(null);
        // When
        var exception = catchException(() -> service.create(identityAttribute));
        // Then
        then(repository).shouldHaveNoInteractions();
        assertThat(exception)
                .asInstanceOf(type(ConstraintViolationException.class))
                .matches(e -> e.getConstraintViolations().size() == violationsCount);
    }

    @ParameterizedTest
    @InstancioSource
    void createBatch_withAtLeastOneInvalidIdentityAttribute_shouldThrowConstraintViolationException(
            List<IdentityAttributeDTO> identityAttributes) {
        // Given
        var violationsCount = 4;
        identityAttributes.getFirst().setId(null);
        identityAttributes.getFirst().setName(null);
        identityAttributes.getFirst().setCode(null);
        identityAttributes.getFirst().setEnabled(null);
        identityAttributes.getFirst().setAssignableToRoles(null);
        // When
        var exception = catchException(() -> service.createBatch(identityAttributes));
        // Then
        then(repository).shouldHaveNoInteractions();
        assertThat(exception)
                .asInstanceOf(type(ConstraintViolationException.class))
                .matches(e -> e.getConstraintViolations().size() == violationsCount);
    }

    @ParameterizedTest
    @InstancioSource
    void update_withAnInvalidIdentityAttribute_shouldThrowConstraintViolationException(
            IdentityAttributeDTO identityAttribute) {
        // Given
        var violationsCount = 4;
        identityAttribute.setId(null);
        identityAttribute.setName(null);
        identityAttribute.setCode(null);
        identityAttribute.setEnabled(null);
        identityAttribute.setAssignableToRoles(null);
        // When
        var exception = catchException(() -> service.update(UUID.randomUUID(), identityAttribute));
        // Then
        then(repository).shouldHaveNoInteractions();
        assertThat(exception)
                .asInstanceOf(type(ConstraintViolationException.class))
                .matches(e -> e.getConstraintViolations().size() == violationsCount);
    }
}
