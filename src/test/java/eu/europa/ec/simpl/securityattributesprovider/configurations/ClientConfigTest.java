package eu.europa.ec.simpl.securityattributesprovider.configurations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestClient;

@ExtendWith(MockitoExtension.class)
class ClientConfigTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    MicroserviceProperties microserviceProperties;

    ClientConfig clientConfig;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    RestClient.Builder builder;

    @BeforeEach
    void setUp() {
        clientConfig = new ClientConfig(microserviceProperties);
    }

    @Test
    void identityProviderParticipantExchange() {
        assertThat(clientConfig.identityProviderParticipantExchange(builder)).isNotNull();
    }

    @Test
    void participantTypeExchange() {
        assertThat(clientConfig.participantTypeExchange(builder)).isNotNull();
    }
}
