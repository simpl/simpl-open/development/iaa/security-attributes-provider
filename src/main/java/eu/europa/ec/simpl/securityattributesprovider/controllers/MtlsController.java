package eu.europa.ec.simpl.securityattributesprovider.controllers;

import eu.europa.ec.simpl.common.constants.SimplHeaders;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.securityattributesprovider.services.IdentityAttributeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("mtls")
public class MtlsController {
    private final IdentityAttributeService identityAttributeService;

    public MtlsController(IdentityAttributeService identityAttributeService) {
        this.identityAttributeService = identityAttributeService;
    }

    @Operation(
            summary = "Get Identity Attributes with Ownership",
            description =
                    "Retrieves a list of identity attributes with ownership information for a given participant ID.",
            parameters = {
                @Parameter(
                        name = SimplHeaders.CREDENTIAL_ID,
                        description = "Participant Public Key Hash",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"),
                        in = ParameterIn.HEADER)
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved identity attributes with ownership",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema =
                                                                @Schema(
                                                                        implementation =
                                                                                IdentityAttributeWithOwnershipDTO
                                                                                        .class))))
            })
    @GetMapping("identity-attribute")
    public List<IdentityAttributeWithOwnershipDTO> getIdentityAttributesWithOwnership(
            @RequestHeader(SimplHeaders.CREDENTIAL_ID) String credentialId) {
        return identityAttributeService.getIdentityAttributesWithOwnership(credentialId);
    }

    @Operation(
            summary = "Get Identity Attributes by Certificate ID",
            description = "Retrieves a list of identity attributes associated with a given certificate ID.",
            parameters = {
                @Parameter(
                        name = "certificateId",
                        description = "Certificate ID used to fetch associated identity attributes",
                        required = true,
                        schema = @Schema(type = "string"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved identity attributes",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema =
                                                                @Schema(implementation = IdentityAttributeDTO.class)))),
                @ApiResponse(responseCode = "404", description = "Certificate ID not found")
            })
    @GetMapping("identity-attribute/{credentialId}")
    public List<IdentityAttributeDTO> getIdentityAttributesByCertificateIdInUri(@PathVariable String credentialId) {
        return identityAttributeService.getIdentityAttributesByCertificateId(credentialId);
    }
}
