package eu.europa.ec.simpl.securityattributesprovider.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SecurityAttributesMappingDTO {
    @NotBlank
    private String name;

    @NotBlank
    private String code;

    @NotBlank
    private String description;

    @NotNull private Boolean assignableToRoles;

    @NotNull @JsonProperty("isRight")
    @NotNull private Boolean right;

    @NotNull private List<String> participantTypes = new ArrayList<>();
}
