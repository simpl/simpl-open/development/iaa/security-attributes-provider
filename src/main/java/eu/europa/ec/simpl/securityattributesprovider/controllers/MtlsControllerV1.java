package eu.europa.ec.simpl.securityattributesprovider.controllers;

import eu.europa.ec.simpl.api.securityattributesprovider.v1.exchanges.MtlsApi;
import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeDTO;
import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.securityattributesprovider.mappers.IdentityAttributeMapperV1;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class MtlsControllerV1 implements MtlsApi {

    private final MtlsController controller;
    private final IdentityAttributeMapperV1 mapper;

    public MtlsControllerV1(MtlsController controller, IdentityAttributeMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public List<IdentityAttributeDTO> getIdentityAttributesByCertificateIdInUri(
            String certificateId, String credentialId) {
        return mapper.toV1(controller.getIdentityAttributesByCertificateIdInUri(credentialId));
    }

    @Override
    public List<IdentityAttributeWithOwnershipDTO> getIdentityAttributesWithOwnership(String credentialId) {
        return mapper.toV1IdentityAttributeWithOwnershipDTO(
                controller.getIdentityAttributesWithOwnership(credentialId));
    }
}
