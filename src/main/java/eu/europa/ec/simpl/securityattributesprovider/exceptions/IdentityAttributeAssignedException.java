package eu.europa.ec.simpl.securityattributesprovider.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class IdentityAttributeAssignedException extends StatusException {

    public IdentityAttributeAssignedException() {
        super(HttpStatus.FORBIDDEN, "The deletion of an assigned identity attribute is not allowed.");
    }
}
