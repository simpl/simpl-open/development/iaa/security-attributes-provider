package eu.europa.ec.simpl.securityattributesprovider.repositories;

import eu.europa.ec.simpl.securityattributesprovider.entities.IdentityAttribute;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.IdentityAttributeNotFoundException;
import eu.europa.ec.simpl.securityattributesprovider.projection.IdentityAttributeWithOwnership;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentityAttributeRepository
        extends JpaRepository<IdentityAttribute, UUID>, JpaSpecificationExecutor<IdentityAttribute> {

    default IdentityAttribute findFullByIdOrThrow(UUID id) {
        return findFullById(id).orElseThrow(() -> new IdentityAttributeNotFoundException(id));
    }

    @EntityGraph(attributePaths = "participantTypes")
    Optional<IdentityAttribute> findFullById(UUID id);

    boolean existsByCodeOrName(String code, String name);

    @Query(
            """
            select i.id from IdentityAttribute i join i.participantTypes p
            where p = :type and i.id not in :excluded and i.enabled is true""")
    Set<UUID> retrieveSnapshot(String type, List<UUID> excluded);

    @Modifying
    @Query("update IdentityAttribute set assignableToRoles = :value where id in :ids")
    void updateAssignableParameter(List<UUID> ids, boolean value);

    @Query(
            """
            select new eu.europa.ec.simpl.securityattributesprovider.projection.IdentityAttributeWithOwnership
            (ia, case when exists(select ia where element(ia.participants).id.participantId = :participantId) then true else false end)
            from IdentityAttribute ia order by ia.id""")
    @EntityGraph(attributePaths = "participantTypes")
    List<IdentityAttributeWithOwnership> findAllWithOwnershipByParticipantId(UUID participantId);

    @Modifying
    @Query(
            value =
                    """
                    insert into identity_attribute_participant_type (identity_attribute_id, participant_type)
                    select id, :#{#participantType}
                    from unnest(:ids) as id
                    """,
            nativeQuery = true)
    void addParticipantType(UUID[] ids, String participantType);

    @Modifying
    @Query(
            value =
                    """
            delete from participant_identity_attribute pia
            where pia.identity_attribute_id in (:identityAttributes)
            and pia.participant_id = :userId""",
            nativeQuery = true)
    void unassignIdentityAttribute(UUID userId, List<UUID> identityAttributes);

    @Modifying
    @Query(
            value =
                    """
            insert into participant_identity_attribute (participant_id, identity_attribute_id)
            select :userId, ida
            from unnest(:identityAttributesId) as ida""",
            nativeQuery = true)
    void assignIdentityAttributes(UUID userId, UUID[] identityAttributesId);
}
