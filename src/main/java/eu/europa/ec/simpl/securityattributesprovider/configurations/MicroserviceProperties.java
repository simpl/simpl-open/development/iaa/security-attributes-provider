package eu.europa.ec.simpl.securityattributesprovider.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "microservice")
public record MicroserviceProperties(IdentityProvider identityProvider, Onboarding onboarding) {

    public record IdentityProvider(String url) {}

    public record Onboarding(String url) {}
}
