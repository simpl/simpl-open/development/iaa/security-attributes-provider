package eu.europa.ec.simpl.securityattributesprovider.configurations;

import java.util.Arrays;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenApiCustomizer customGlobalResponses() {
        var statusCodesToClear = Arrays.asList("400", "401", "403", "404", "409");
        return openApi -> openApi.getPaths()
                .forEach((path, pathItem) -> pathItem.readOperations().forEach(operation -> {
                    if (!(path.endsWith("/identity-attribute") && pathItem.getPost() != null)) {
                        operation.getResponses().remove("409");
                    }
                    for (String statusCode : statusCodesToClear) {
                        var response = operation.getResponses().get(statusCode);
                        if (response != null) {
                            response.setContent(null);
                        }
                    }
                }));
    }
}
