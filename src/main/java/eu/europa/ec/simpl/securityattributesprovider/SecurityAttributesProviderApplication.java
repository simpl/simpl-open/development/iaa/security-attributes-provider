package eu.europa.ec.simpl.securityattributesprovider;

import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import org.springdoc.core.utils.SpringDocUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class SecurityAttributesProviderApplication {
    public static void main(String[] args) {
        SpringDocUtils.getConfig().addParentType(IdentityAttributeWithOwnershipDTO.class.getSimpleName());
        SpringApplication.run(SecurityAttributesProviderApplication.class, args);
    }
}
