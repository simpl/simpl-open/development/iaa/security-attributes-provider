package eu.europa.ec.simpl.securityattributesprovider.services.impl;

import eu.europa.ec.simpl.common.exchanges.identityprovider.IdentityProviderParticipantExchange;
import eu.europa.ec.simpl.common.filters.IdentityAttributeFilter;
import eu.europa.ec.simpl.common.filters.ParticipantFilter;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.common.model.validators.CreateOperation;
import eu.europa.ec.simpl.common.model.validators.UpdateOperation;
import eu.europa.ec.simpl.securityattributesprovider.entities.IdentityAttribute;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.IdentityAttributeAlreadyExistException;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.IdentityAttributeAssignedException;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.IdentityAttributeNotFoundException;
import eu.europa.ec.simpl.securityattributesprovider.mappers.IdentityAttributeMapper;
import eu.europa.ec.simpl.securityattributesprovider.repositories.IdentityAttributeRepository;
import eu.europa.ec.simpl.securityattributesprovider.repositories.specifications.IdentityAttributeSpecification;
import eu.europa.ec.simpl.securityattributesprovider.services.IdentityAttributeService;
import eu.europa.ec.simpl.securityattributesprovider.services.ParticipantTypeService;
import jakarta.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class IdentityAttributeServiceImpl implements IdentityAttributeService {

    private final IdentityAttributeRepository repository;
    private final IdentityAttributeMapper mapper;
    private final IdentityProviderParticipantExchange participantExchange;
    private final ParticipantTypeService participantTypeService;

    public IdentityAttributeServiceImpl(
            IdentityAttributeRepository repository,
            IdentityAttributeMapper mapper,
            IdentityProviderParticipantExchange participantExchange,
            ParticipantTypeService participantTypeService) {
        this.repository = repository;
        this.mapper = mapper;
        this.participantExchange = participantExchange;
        this.participantTypeService = participantTypeService;
    }

    @Override
    @Validated(CreateOperation.class)
    public IdentityAttributeDTO create(@Valid IdentityAttributeDTO attributeDTO) {
        log.info("create: creating identity attribute [{}]", attributeDTO);
        if (repository.existsByCodeOrName(attributeDTO.getCode(), attributeDTO.getName())) {
            throw new IdentityAttributeAlreadyExistException(attributeDTO.getCode(), attributeDTO.getName());
        }
        var saved = doCreate(List.of(attributeDTO));
        return mapper.toDto(saved.getFirst());
    }

    @Override
    @Transactional
    @Validated(CreateOperation.class)
    public void createBatch(@Valid List<IdentityAttributeDTO> identityAttributes) {
        log.info("storing {} identity attributes...", identityAttributes.size());
        var saved = doCreate(identityAttributes);
        log.info("{} new identity attributes stored.", saved.size());
    }

    private List<IdentityAttribute> doCreate(List<IdentityAttributeDTO> identityAttributes) {
        var participantTypes = identityAttributes.stream()
                .flatMap(identityAttribute -> identityAttribute.getParticipantTypes() != null
                        ? identityAttribute.getParticipantTypes().stream()
                        : Stream.empty())
                .collect(Collectors.toSet());

        participantTypeService.checkExistingOrThrow(participantTypes);

        var identityAttributesToSave =
                identityAttributes.stream().map(mapper::toEntity).toList();
        return repository.saveAll(identityAttributesToSave);
    }

    @Override
    public IdentityAttributeDTO findById(UUID id) {
        log.info("findById: retrieving identity attribute [{}]", id);
        return repository
                .findFullById(id)
                .map(mapper::toDto)
                .orElseThrow(() -> new IdentityAttributeNotFoundException(id));
    }

    @Override
    @Transactional
    @Validated(UpdateOperation.class)
    public void update(UUID id, @Valid IdentityAttributeDTO attributeDTO) {
        log.info("update: updating identity attribute properties [{}]", attributeDTO);
        participantTypeService.checkExistingOrThrow(attributeDTO.getParticipantTypes());
        var identityAttribute = repository.findFullByIdOrThrow(id);
        mapper.updateEntity(attributeDTO, identityAttribute);
    }

    @Override
    public void delete(UUID id) {
        log.info("delete: deleting identity attribute with id [{}]", id);
        var optAttr = repository.findById(id).orElseThrow(() -> new IdentityAttributeNotFoundException(id));
        if (optAttr.isUsed()) {
            throw new IdentityAttributeAssignedException();
        }
        repository.deleteById(id);
    }

    @Override
    public Page<IdentityAttributeDTO> search(IdentityAttributeFilter request, Pageable pageable) {
        log.info("search: searching identity attribute with parameters [{}]", request);
        return repository
                .findAll(new IdentityAttributeSpecification(request), pageable)
                .map(mapper::toLightDto);
    }

    @Override
    @Transactional
    public void updateAssignableParameter(List<UUID> request, boolean value) {
        request.stream()
                .distinct()
                .filter(id -> !repository.existsById(id))
                .findFirst()
                .ifPresent(id -> {
                    throw new IdentityAttributeNotFoundException(id);
                });
        repository.updateAssignableParameter(request, value);
    }

    @Override
    public List<IdentityAttributeWithOwnershipDTO> getIdentityAttributesWithOwnership(String credentialId) {
        var participantId = findParticipantIdByCredentialId(credentialId).orElseThrow();
        return repository.findAllWithOwnershipByParticipantId(participantId).stream()
                .distinct()
                .map(mapper::toDtoWithOwnership)
                .toList();
    }

    @Override
    public List<IdentityAttributeDTO> getIdentityAttributesByCertificateId(String certificateId) {
        var id = findParticipantIdByCredentialId(certificateId);
        return id.map(uuid -> search(new IdentityAttributeFilter().setParticipantIdIn(uuid), Pageable.unpaged())
                        .getContent())
                .orElseGet(ArrayList::new);
    }

    @Override
    @Transactional
    public void addParticipantType(List<UUID> request, String participantType) {
        participantTypeService.checkExistingOrThrow(Set.of(participantType));

        repository.addParticipantType(request.toArray(UUID[]::new), participantType);
    }

    @Override
    @Transactional
    public void unassign(UUID participantId, List<UUID> identityAttributes) {
        log.info("ParticipantService.unassign() identity attributes for participant {}", participantId);
        repository.unassignIdentityAttribute(participantId, identityAttributes);
    }

    @Override
    @Transactional
    public void assign(UUID participantId, List<UUID> identityAttributes) {
        log.info("ParticipantService.assign() identity attributes for participant {}", participantId);
        repository.assignIdentityAttributes(participantId, identityAttributes.toArray(UUID[]::new));
    }

    @Transactional
    @Override
    public void assignLastSnapshot(String participantType, UUID participantId, List<UUID> excludedAttributes) {
        var attributesToAssign = repository.retrieveSnapshot(participantType, excludedAttributes);
        repository.assignIdentityAttributes(participantId, attributesToAssign.toArray(UUID[]::new));
    }

    private Optional<UUID> findParticipantIdByCredentialId(String credentialId) {
        return participantExchange
                .search(new ParticipantFilter().setCredentialId(credentialId), Pageable.ofSize(1))
                .stream()
                .findFirst()
                .map(ParticipantDTO::getId);
    }
}
