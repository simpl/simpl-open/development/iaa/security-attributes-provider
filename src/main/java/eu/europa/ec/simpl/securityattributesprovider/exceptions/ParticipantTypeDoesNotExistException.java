package eu.europa.ec.simpl.securityattributesprovider.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.Set;
import org.springframework.http.HttpStatus;

public class ParticipantTypeDoesNotExistException extends StatusException {

    public ParticipantTypeDoesNotExistException(Set<String> participantTypes) {
        super(
                HttpStatus.BAD_REQUEST,
                "Non-existing participant type(s): %s.".formatted(String.join(", ", participantTypes)));
    }
}
