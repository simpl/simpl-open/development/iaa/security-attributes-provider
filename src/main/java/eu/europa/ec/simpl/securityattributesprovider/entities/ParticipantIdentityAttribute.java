package eu.europa.ec.simpl.securityattributesprovider.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Entity
@Accessors(chain = true)
@Table(name = "participant_identity_attribute")
public class ParticipantIdentityAttribute {
    @EmbeddedId
    private ParticipantIdentityAttributeId id;

    @MapsId("identityAttributeId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "identity_attribute_id", nullable = false)
    private IdentityAttribute identityAttribute;
}
