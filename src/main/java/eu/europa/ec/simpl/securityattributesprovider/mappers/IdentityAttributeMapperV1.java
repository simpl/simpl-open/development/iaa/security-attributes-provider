package eu.europa.ec.simpl.securityattributesprovider.mappers;

import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.PageResponseIdentityAttributeDTO;
import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.SearchFilterParameterDTO;
import eu.europa.ec.simpl.common.filters.IdentityAttributeFilter;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface IdentityAttributeMapperV1 {
    IdentityAttributeDTO toV0(
            eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeDTO identityAttributeDTO);

    eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeDTO toV1(
            IdentityAttributeDTO identityAttributeDTO);

    IdentityAttributeFilter toV0(SearchFilterParameterDTO filter);

    PageResponseIdentityAttributeDTO toV1(PageResponse<IdentityAttributeDTO> search);

    List<IdentityAttributeWithOwnershipDTO> toV1IdentityAttributeWithOwnershipDTO(
            List<eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO>
                    identityAttributesWithOwnership);

    @Mapping(target = "id", source = "identityAttribute.id")
    @Mapping(target = "code", source = "identityAttribute.code")
    @Mapping(target = "name", source = "identityAttribute.name")
    @Mapping(target = "description", source = "identityAttribute.description")
    @Mapping(target = "assignableToRoles", source = "identityAttribute.assignableToRoles")
    @Mapping(target = "enabled", source = "identityAttribute.enabled")
    @Mapping(target = "creationTimestamp", source = "identityAttribute.creationTimestamp")
    @Mapping(target = "updateTimestamp", source = "identityAttribute.updateTimestamp")
    @Mapping(target = "participantTypes", source = "identityAttribute.participantTypes")
    @Mapping(target = "used", source = "identityAttribute.used")
    eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeWithOwnershipDTO toV1(
            eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO
                    identityAttributeWithOwnershipDTO);

    List<eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeDTO> toV1(
            List<IdentityAttributeDTO> identityAttributesByCertificateIdInUri);

    List<IdentityAttributeDTO> toV0(
            List<eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeDTO> identityAttributeDTO);
}
