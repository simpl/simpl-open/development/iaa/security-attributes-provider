package eu.europa.ec.simpl.securityattributesprovider.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.*;
import java.time.Instant;
import java.util.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "identity_attribute")
@Accessors(chain = true)
@Getter
@Setter
@ToString
public class IdentityAttribute {

    @Id
    @UUIDv7Generator
    private UUID id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ToString.Exclude
    @ElementCollection
    @CollectionTable(
            name = "identity_attribute_participant_type",
            joinColumns = @JoinColumn(name = "identity_attribute_id"))
    @Column(name = "participant_type")
    private Set<String> participantTypes = new HashSet<>();

    @Column(name = "assignable_to_roles")
    private boolean assignableToRoles;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "is_right")
    private boolean right;

    @CreationTimestamp
    @Column(name = "creation_timestamp")
    private Instant creationTimestamp;

    @UpdateTimestamp
    @Column(name = "update_timestamp")
    private Instant updateTimestamp;

    @Formula("(select exists(select 1 from participant_identity_attribute pia where pia.identity_attribute_id = id))")
    private boolean used;

    @OneToMany(mappedBy = "identityAttribute")
    private Set<ParticipantIdentityAttribute> participants = new LinkedHashSet<>();
}
