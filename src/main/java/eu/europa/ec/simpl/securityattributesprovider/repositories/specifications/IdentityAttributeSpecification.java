package eu.europa.ec.simpl.securityattributesprovider.repositories.specifications;

import eu.europa.ec.simpl.common.filters.IdentityAttributeFilter;
import eu.europa.ec.simpl.securityattributesprovider.entities.*;
import eu.europa.ec.simpl.securityattributesprovider.entities.IdentityAttribute;
import eu.europa.ec.simpl.securityattributesprovider.entities.ParticipantIdentityAttribute;
import jakarta.persistence.criteria.*;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.jpa.domain.Specification;

public class IdentityAttributeSpecification implements Specification<IdentityAttribute> {
    private static final String LIKE_TEMPLATE = "%%%s%%";

    private final transient IdentityAttributeFilter filter;

    public IdentityAttributeSpecification(IdentityAttributeFilter filter) {
        this.filter = filter;
    }

    private static Specification<IdentityAttribute> participantTypeIn(String participantType) {
        return (root, query, criteriaBuilder) -> participantType != null
                ? criteriaBuilder.equal(root.join(IdentityAttribute_.participantTypes), participantType)
                : null;
    }

    private static Specification<IdentityAttribute> participantTypeNotIn(String participantType) {
        return (root, query, criteriaBuilder) -> {
            if (participantType == null) {
                return null;
            }
            var subQuery = query.subquery(Integer.class);
            var subRoot = subQuery.from(IdentityAttribute.class);
            subQuery.select(criteriaBuilder.literal(1));
            subQuery.where(
                    criteriaBuilder.equal(subRoot.join(IdentityAttribute_.participantTypes), participantType),
                    criteriaBuilder.equal(subRoot.get(IdentityAttribute_.id), root.get(IdentityAttribute_.id)));
            return criteriaBuilder.not(criteriaBuilder.exists(subQuery));
        };
    }

    private static Specification<IdentityAttribute> codeLike(String code) {
        return (root, query, criteriaBuilder) -> code != null
                ? criteriaBuilder.like(
                        criteriaBuilder.lower(root.get(IdentityAttribute_.code)),
                        LIKE_TEMPLATE.formatted(code.toLowerCase()))
                : null;
    }

    private static Specification<IdentityAttribute> nameLike(String name) {
        return (root, query, criteriaBuilder) -> name != null
                ? criteriaBuilder.like(
                        criteriaBuilder.lower(root.get(IdentityAttribute_.name)),
                        LIKE_TEMPLATE.formatted(name.toLowerCase()))
                : null;
    }

    private static Specification<IdentityAttribute> enabledEqual(Boolean enabled) {
        return (root, query, criteriaBuilder) ->
                enabled != null ? criteriaBuilder.equal(root.get(IdentityAttribute_.enabled), enabled) : null;
    }

    private static Specification<IdentityAttribute> participantIdIn(UUID participantId) {
        return (root, query, criteriaBuilder) -> participantId != null
                ? criteriaBuilder.equal(
                        root.join(IdentityAttribute_.participants)
                                .get(ParticipantIdentityAttribute_.id)
                                .get(ParticipantIdentityAttributeId_.participantId),
                        participantId)
                : null;
    }

    private static Specification<IdentityAttribute> participantIdNotIn(UUID participantId) {
        return (root, query, criteriaBuilder) -> {
            if (participantId == null) {
                return null;
            }
            var subQuery = query.subquery(Integer.class);
            var subRoot = subQuery.from(ParticipantIdentityAttribute.class);
            subQuery.select(criteriaBuilder.literal(1));
            subQuery.where(
                    criteriaBuilder.equal(
                            subRoot.get(ParticipantIdentityAttribute_.id)
                                    .get(ParticipantIdentityAttributeId_.participantId),
                            participantId),
                    criteriaBuilder.equal(
                            subRoot.get(ParticipantIdentityAttribute_.id)
                                    .get(ParticipantIdentityAttributeId_.identityAttributeId),
                            root.get(IdentityAttribute_.id)));
            return criteriaBuilder.not(criteriaBuilder.exists(subQuery));
        };
    }

    private static Specification<IdentityAttribute> updateTimestampGreaterThanOrEqualTo(Instant updateTimestampMin) {
        return (root, query, criteriaBuilder) -> updateTimestampMin != null
                ? criteriaBuilder.greaterThanOrEqualTo(root.get(IdentityAttribute_.updateTimestamp), updateTimestampMin)
                : null;
    }

    private static Specification<IdentityAttribute> updateTimestampLessThanOrEqualTo(Instant updateTimestampMax) {

        return (root, query, criteriaBuilder) -> updateTimestampMax != null
                ? criteriaBuilder.lessThanOrEqualTo(root.get(IdentityAttribute_.updateTimestamp), updateTimestampMax)
                : null;
    }

    @Override
    public Predicate toPredicate(
            Root<IdentityAttribute> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return Specification.allOf(
                        participantTypeIn(filter.getParticipantTypeIn()),
                        participantTypeNotIn(filter.getParticipantTypeNotIn()),
                        codeLike(filter.getCode()),
                        nameLike(filter.getName()),
                        enabledEqual(filter.getEnabled()),
                        participantIdIn(filter.getParticipantIdIn()),
                        participantIdNotIn(filter.getParticipantIdNotIn()),
                        updateTimestampGreaterThanOrEqualTo(filter.getUpdateTimestampFrom()),
                        updateTimestampLessThanOrEqualTo(filter.getUpdateTimestampTo()))
                .toPredicate(root, query, criteriaBuilder);
    }
}
