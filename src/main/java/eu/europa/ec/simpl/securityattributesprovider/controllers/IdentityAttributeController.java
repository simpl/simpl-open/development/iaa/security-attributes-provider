package eu.europa.ec.simpl.securityattributesprovider.controllers;

import eu.europa.ec.simpl.common.exchanges.securityattributeprovider.IdentityAttributeExchange;
import eu.europa.ec.simpl.common.filters.IdentityAttributeFilter;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.validators.UpdateOperation;
import eu.europa.ec.simpl.common.responses.PageResponse;
import eu.europa.ec.simpl.securityattributesprovider.services.IdentityAttributeService;
import java.util.List;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
public class IdentityAttributeController implements IdentityAttributeExchange {
    private final IdentityAttributeService service;

    public IdentityAttributeController(IdentityAttributeService service) {
        this.service = service;
    }

    @Override
    public IdentityAttributeDTO create(@RequestBody IdentityAttributeDTO attribute) {
        return service.create(attribute);
    }

    @Override
    public IdentityAttributeDTO findById(@PathVariable UUID id) {
        return service.findById(id);
    }

    @Override
    public void updateAttributes(
            @PathVariable UUID id, @RequestBody @Validated(UpdateOperation.class) IdentityAttributeDTO attribute) {
        attribute.setId(id);
        service.update(id, attribute);
    }

    @Override
    public void delete(@PathVariable UUID id) {
        service.delete(id);
    }

    @Override
    public PageResponse<IdentityAttributeDTO> search(
            @ParameterObject @ModelAttribute IdentityAttributeFilter filter,
            @PageableDefault(sort = "id") @ParameterObject Pageable pageable) {
        return new PageResponse<>(service.search(filter, pageable));
    }

    @Override
    public void updateAssignableParameter(@RequestBody List<UUID> body, @PathVariable boolean value) {
        service.updateAssignableParameter(body, value);
    }

    @Override
    public void addParticipantType(@RequestBody List<UUID> body, @PathVariable String participantType) {
        service.addParticipantType(body, participantType);
    }

    @Override
    public void unassign(UUID participantId, List<UUID> identityAttributes) {
        service.unassign(participantId, identityAttributes);
    }

    @Override
    public void assign(UUID participantId, List<UUID> identityAttributes) {
        service.assign(participantId, identityAttributes);
    }

    @Override
    @SuppressWarnings("all")
    public void assignSnapshot(String participantType, UUID participantId, List<UUID> excludedAttributes) {
        service.assignLastSnapshot(participantType, participantId, excludedAttributes);
    }

    @Override
    public void importIdentityAttributes(@RequestBody List<IdentityAttributeDTO> list) {
        log.info("Initializing identity attributes from CLI");
        service.createBatch(list);
    }
}
