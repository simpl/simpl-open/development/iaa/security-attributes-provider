package eu.europa.ec.simpl.securityattributesprovider.services.impl;

import eu.europa.ec.simpl.common.exchanges.onboarding.ParticipantTypeExchange;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.ParticipantTypeDoesNotExistException;
import eu.europa.ec.simpl.securityattributesprovider.services.ParticipantTypeService;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class ParticipantTypeServiceImpl implements ParticipantTypeService {

    private final ParticipantTypeExchange participantTypeExchange;

    public ParticipantTypeServiceImpl(ParticipantTypeExchange participantTypeExchange) {
        this.participantTypeExchange = participantTypeExchange;
    }

    @Override
    public void checkExistingOrThrow(@NonNull Set<String> participantTypes) {

        if (participantTypes.isEmpty()) {
            return;
        }

        var existingParticipantTypes = participantTypeExchange.getParticipantTypes().stream()
                .map(ParticipantTypeDTO::getValue)
                .collect(Collectors.toSet());

        var missingParticipantTypes = new HashSet<>(participantTypes);
        missingParticipantTypes.removeAll(existingParticipantTypes);

        if (!missingParticipantTypes.isEmpty()) {
            throw new ParticipantTypeDoesNotExistException(missingParticipantTypes);
        }
    }
}
