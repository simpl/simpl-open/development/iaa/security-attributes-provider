package eu.europa.ec.simpl.securityattributesprovider.services;

import eu.europa.ec.simpl.common.filters.IdentityAttributeFilter;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.common.model.validators.CreateOperation;
import eu.europa.ec.simpl.common.model.validators.UpdateOperation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

public interface IdentityAttributeService {
    @Validated(CreateOperation.class)
    IdentityAttributeDTO create(@Valid IdentityAttributeDTO attributeDTO);

    @Transactional
    @Validated(CreateOperation.class)
    void createBatch(@Valid List<IdentityAttributeDTO> list);

    IdentityAttributeDTO findById(UUID id);

    @Transactional
    @Validated(UpdateOperation.class)
    void update(UUID id, @Valid IdentityAttributeDTO attributeDTO);

    void delete(UUID id);

    Page<IdentityAttributeDTO> search(IdentityAttributeFilter request, Pageable pageable);

    @Transactional
    void updateAssignableParameter(@NotEmpty List<UUID> request, boolean value);

    List<IdentityAttributeWithOwnershipDTO> getIdentityAttributesWithOwnership(String certificateId);

    List<IdentityAttributeDTO> getIdentityAttributesByCertificateId(String credentialId);

    @Transactional
    void addParticipantType(List<UUID> request, String participantType);

    @Transactional
    void unassign(UUID participantId, List<UUID> identityAttributes);

    @Transactional
    void assign(UUID participantId, List<UUID> identityAttributes);

    @Transactional
    void assignLastSnapshot(String participantType, UUID participantId, List<UUID> excludedAttributes);
}
