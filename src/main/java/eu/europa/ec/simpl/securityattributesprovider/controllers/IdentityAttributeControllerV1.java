package eu.europa.ec.simpl.securityattributesprovider.controllers;

import eu.europa.ec.simpl.api.securityattributesprovider.v1.exchanges.IdentityAttributesApi;
import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttributeDTO;
import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.PageResponseIdentityAttributeDTO;
import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.SearchFilterParameterDTO;
import eu.europa.ec.simpl.securityattributesprovider.mappers.IdentityAttributeMapperV1;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class IdentityAttributeControllerV1 implements IdentityAttributesApi {

    private final IdentityAttributeController controller;
    private final IdentityAttributeMapperV1 mapper;

    public IdentityAttributeControllerV1(IdentityAttributeController controller, IdentityAttributeMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public void addParticipantType(String participantType, List<UUID> UUID) {
        controller.addParticipantType(UUID, participantType);
    }

    @Override
    public void assign(UUID participantId, List<UUID> UUID) {
        controller.assign(participantId, UUID);
    }

    @Override
    public void assignDeprecated(UUID participantId, List<UUID> UUID) {
        assign(participantId, UUID);
    }

    @Override
    public void assignSnapshot(String participantType, UUID participantId, List<UUID> UUID) {
        controller.assignSnapshot(participantType, participantId, UUID);
    }

    @Override
    public IdentityAttributeDTO create(IdentityAttributeDTO identityAttributeDTO) {
        return mapper.toV1(controller.create(mapper.toV0(identityAttributeDTO)));
    }

    @Override
    public void delete(UUID id) {
        controller.delete(id);
    }

    @Override
    public IdentityAttributeDTO findById(UUID id) {
        return mapper.toV1(controller.findById(id));
    }

    @Override
    public void importIdentityAttributes(List<IdentityAttributeDTO> identityAttributeDTO) {
        controller.importIdentityAttributes(mapper.toV0(identityAttributeDTO));
    }

    @Override
    public PageResponseIdentityAttributeDTO search(
            Integer page, Integer size, List<String> sort, SearchFilterParameterDTO filter) {
        return mapper.toV1(controller.search(
                mapper.toV0(filter), PageRequest.of(page, size, Sort.by(sort.toArray(String[]::new)))));
    }

    @Override
    public void unassign(UUID participantId, List<UUID> UUID) {
        controller.unassign(participantId, UUID);
    }

    @Override
    public void unassignDeprecated(UUID participantId, List<UUID> UUID) {
        unassign(participantId, UUID);
    }

    @Override
    public void updateAssignableParameter(Boolean value, List<UUID> UUID) {
        controller.updateAssignableParameter(UUID, value);
    }

    @Override
    public void updateAttributes(UUID id, IdentityAttributeDTO identityAttributeDTO) {
        controller.updateAttributes(id, mapper.toV0(identityAttributeDTO));
    }
}
