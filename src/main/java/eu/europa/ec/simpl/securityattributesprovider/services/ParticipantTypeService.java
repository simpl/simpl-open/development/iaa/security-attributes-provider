package eu.europa.ec.simpl.securityattributesprovider.services;

import java.util.Set;

public interface ParticipantTypeService {
    void checkExistingOrThrow(Set<String> participantTypes);
}
