package eu.europa.ec.simpl.securityattributesprovider.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class IdentityAttributeAlreadyExistException extends StatusException {

    protected IdentityAttributeAlreadyExistException(HttpStatus status, String code, String name) {
        super(status, "Identity attribute with code [ %s ] or name [ %s ] already exist".formatted(code, name));
    }

    public IdentityAttributeAlreadyExistException(String code, String name) {
        this(HttpStatus.CONFLICT, code, name);
    }
}
