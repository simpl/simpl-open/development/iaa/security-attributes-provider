package eu.europa.ec.simpl.securityattributesprovider.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.Hibernate;

@Getter
@Setter
@Accessors(chain = true)
@Embeddable
public class ParticipantIdentityAttributeId implements java.io.Serializable {
    private static final long serialVersionUID = -2157962239188767845L;

    @NotNull @Column(name = "participant_id", nullable = false)
    private UUID participantId;

    @NotNull @Column(name = "identity_attribute_id", nullable = false)
    private UUID identityAttributeId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ParticipantIdentityAttributeId entity = (ParticipantIdentityAttributeId) o;
        return Objects.equals(this.participantId, entity.participantId)
                && Objects.equals(this.identityAttributeId, entity.identityAttributeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(participantId, identityAttributeId);
    }
}
