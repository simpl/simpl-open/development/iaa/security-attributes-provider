package eu.europa.ec.simpl.securityattributesprovider.mappers;

import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.securityattributesprovider.dto.SecurityAttributesMappingDTO;
import eu.europa.ec.simpl.securityattributesprovider.entities.IdentityAttribute;
import eu.europa.ec.simpl.securityattributesprovider.projection.IdentityAttributeWithOwnership;
import org.mapstruct.*;

@Mapper
public interface IdentityAttributeMapper {

    @Mapping(target = "enabled", constant = "true")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "participants", ignore = true)
    @Mapping(target = "creationTimestamp", ignore = true)
    @Mapping(target = "updateTimestamp", ignore = true)
    IdentityAttribute toEntity(SecurityAttributesMappingDTO attribute);

    IdentityAttribute toEntity(IdentityAttributeDTO dto);

    IdentityAttributeDTO toDto(IdentityAttribute entity);

    @Mapping(target = "identityAttribute")
    IdentityAttributeWithOwnershipDTO toDtoWithOwnership(IdentityAttributeWithOwnership projection);

    @Named("light")
    @Mapping(target = "participantTypes", ignore = true)
    IdentityAttributeDTO toLightDto(IdentityAttribute entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "creationTimestamp", ignore = true)
    @Mapping(target = "updateTimestamp", ignore = true)
    void updateEntity(IdentityAttributeDTO dto, @MappingTarget IdentityAttribute entity);
}
