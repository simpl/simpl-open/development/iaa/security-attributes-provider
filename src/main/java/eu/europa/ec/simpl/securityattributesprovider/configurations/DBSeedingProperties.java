package eu.europa.ec.simpl.securityattributesprovider.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "database-seeding")
public record DBSeedingProperties(SecurityAttributeProviderMapping securityAttributeProviderMapping) {
    public record SecurityAttributeProviderMapping(String filePath, boolean enabled) {}
}
