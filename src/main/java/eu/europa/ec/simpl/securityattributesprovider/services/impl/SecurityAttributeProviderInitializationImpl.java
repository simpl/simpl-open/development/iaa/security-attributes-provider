package eu.europa.ec.simpl.securityattributesprovider.services.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.exchanges.onboarding.ParticipantTypeExchange;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.securityattributesprovider.configurations.DBSeedingProperties;
import eu.europa.ec.simpl.securityattributesprovider.dto.SecurityAttributesMappingDTO;
import eu.europa.ec.simpl.securityattributesprovider.exceptions.ParticipantTypeDoesNotExistException;
import eu.europa.ec.simpl.securityattributesprovider.mappers.IdentityAttributeMapper;
import eu.europa.ec.simpl.securityattributesprovider.repositories.IdentityAttributeRepository;
import eu.europa.ec.simpl.securityattributesprovider.services.SecurityAttributeProviderInitialization;
import jakarta.annotation.PostConstruct;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class SecurityAttributeProviderInitializationImpl implements SecurityAttributeProviderInitialization {

    private final DBSeedingProperties props;
    private final ResourceLoader resourceLoader;
    private final Validator validator;
    private final IdentityAttributeRepository identityAttributeRepository;
    private final IdentityAttributeMapper identityAttributeMapper;
    private final ParticipantTypeExchange participantTypeExchange;

    public SecurityAttributeProviderInitializationImpl(
            DBSeedingProperties props,
            ResourceLoader resourceLoader,
            Validator validator,
            IdentityAttributeRepository identityAttributeRepository,
            IdentityAttributeMapper identityAttributeMapper,
            ParticipantTypeExchange participantTypeExchange) {
        this.props = props;
        this.resourceLoader = resourceLoader;
        this.validator = validator;
        this.identityAttributeRepository = identityAttributeRepository;
        this.identityAttributeMapper = identityAttributeMapper;
        this.participantTypeExchange = participantTypeExchange;
    }

    @PostConstruct
    public void init() {
        if (props.securityAttributeProviderMapping().enabled()) {
            if (identityAttributeRepository.count() == 0) {
                try {
                    var attributes = getSecurityAttributeProviderInitData();
                    validateAttributes(attributes);
                    var validParticipantTypes = getParticipantTypesFromOnboarding();
                    validateParticipantTypes(attributes, validParticipantTypes);
                    storeIdentityAttributes(attributes);
                } catch (IOException e) {
                    log.error("Unable to process attributes", e);
                }
                log.info("End initialization attributes");
            } else {
                log.info("Identity attribute respository is not empty. Skip initialization process");
            }
        } else {
            log.info("Skip initialization identity attributes");
        }
    }

    private List<String> getParticipantTypesFromOnboarding() {
        log.info("Retrieve participantTypes from onboarding microservices");
        return participantTypeExchange.getParticipantTypes().stream()
                .map(ParticipantTypeDTO::getValue)
                .toList();
    }

    private void validateParticipantTypes(
            List<SecurityAttributesMappingDTO> attributes, List<String> validParticipantTypes) {
        log.info("Validation participant types");
        var invalidMappings = attributes.stream()
                .flatMap(attribute -> getInvalidParticipantTypes(attribute, validParticipantTypes))
                .collect(Collectors.toSet());

        if (!invalidMappings.isEmpty()) {
            throw new ParticipantTypeDoesNotExistException(invalidMappings);
        }
    }

    private Stream<String> getInvalidParticipantTypes(
            SecurityAttributesMappingDTO mapping, List<String> validParticipantTypes) {
        var invalidParticipantTypes = mapping.getParticipantTypes().stream()
                .filter(Predicate.not(validParticipantTypes::contains))
                .toList();

        if (!invalidParticipantTypes.isEmpty()) {
            log.error(
                    "Invalid participant types [{}] in attribute {}", mapping.getParticipantTypes(), mapping.getName());
        }
        return invalidParticipantTypes.stream();
    }

    private List<SecurityAttributesMappingDTO> getSecurityAttributeProviderInitData() throws IOException {
        var filePath = this.props.securityAttributeProviderMapping().filePath();
        try (InputStream is = resourceLoader.getResource(filePath).getInputStream()) {
            var mappings = new ObjectMapper().readValue(is, new TypeReference<List<SecurityAttributesMappingDTO>>() {});
            log.info("Reading identity attributes from file system mappings from file system. File: {}", filePath);
            return mappings;
        }
    }

    private void validateAttributes(List<SecurityAttributesMappingDTO> mappings) {
        log.info("Start validation security attributes");
        mappings.forEach(mapping -> {
            var violations = this.validator.validate(mapping);
            if (!violations.isEmpty()) {
                violations.forEach(violation ->
                        log.error("Validation failed for mapping: {} - {}", mapping, violation.getMessage()));
                throw new ConstraintViolationException(violations);
            }
        });
    }

    private void storeIdentityAttributes(List<SecurityAttributesMappingDTO> attributes) {
        log.info("Start store all identity attributes");
        attributes.forEach(this::storeIdentityAttribute);
    }

    private void storeIdentityAttribute(SecurityAttributesMappingDTO attribute) {
        log.info("Initialize attribute {}", attribute);
        var attributeEntity = identityAttributeMapper.toEntity(attribute);
        identityAttributeRepository.save(attributeEntity);
        log.info("Read attribute {}", attributeEntity.getId());
    }
}
