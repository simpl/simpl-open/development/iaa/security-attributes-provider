package eu.europa.ec.simpl.securityattributesprovider.services;

public interface SecurityAttributeProviderInitialization {
    void init();
}
