package eu.europa.ec.simpl.securityattributesprovider.projection;

import eu.europa.ec.simpl.securityattributesprovider.entities.IdentityAttribute;
import java.util.Objects;

public record IdentityAttributeWithOwnership(IdentityAttribute identityAttribute, boolean assignedToParticipant) {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentityAttributeWithOwnership that = (IdentityAttributeWithOwnership) o;
        return Objects.equals(identityAttribute.getId(), that.identityAttribute.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(identityAttribute.getId());
    }
}
