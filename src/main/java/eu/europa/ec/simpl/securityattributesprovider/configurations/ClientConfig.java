package eu.europa.ec.simpl.securityattributesprovider.configurations;

import eu.europa.ec.simpl.common.argumentresolvers.PageableArgumentResolver;
import eu.europa.ec.simpl.common.argumentresolvers.QueryParamsArgumentResolver;
import eu.europa.ec.simpl.common.exchanges.identityprovider.IdentityProviderParticipantExchange;
import eu.europa.ec.simpl.common.exchanges.onboarding.ParticipantTypeExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class ClientConfig {

    private final MicroserviceProperties microserviceProperties;

    public ClientConfig(MicroserviceProperties microserviceProperties) {
        this.microserviceProperties = microserviceProperties;
    }

    @Bean
    public IdentityProviderParticipantExchange identityProviderParticipantExchange(
            RestClient.Builder restClientBuilder) {
        return buildExchange(
                microserviceProperties.identityProvider().url(),
                restClientBuilder,
                IdentityProviderParticipantExchange.class);
    }

    @Bean
    public ParticipantTypeExchange participantTypeExchange(RestClient.Builder restClientBuilder) {
        return buildExchange(
                microserviceProperties.onboarding().url(), restClientBuilder, ParticipantTypeExchange.class);
    }

    private <E> E buildExchange(String baseurl, RestClient.Builder restClientBuilder, Class<E> clazz) {
        var restClient = restClientBuilder.baseUrl(baseurl).build();
        var adapter = RestClientAdapter.create(restClient);
        var factory = HttpServiceProxyFactory.builderFor(adapter)
                .customArgumentResolver(new PageableArgumentResolver())
                .customArgumentResolver(new QueryParamsArgumentResolver())
                .build();
        return factory.createClient(clazz);
    }
}
