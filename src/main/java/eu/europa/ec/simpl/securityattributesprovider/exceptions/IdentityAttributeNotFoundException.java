package eu.europa.ec.simpl.securityattributesprovider.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class IdentityAttributeNotFoundException extends StatusException {
    public IdentityAttributeNotFoundException(UUID id) {
        this(HttpStatus.NOT_FOUND, id);
    }

    public IdentityAttributeNotFoundException(HttpStatus status, UUID id) {
        super(status, "Identity attribute with id [ %s ] not found".formatted(id.toString()));
    }
}
